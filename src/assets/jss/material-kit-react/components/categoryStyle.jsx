const categoryStyle = () => ({
  catul: {
    listStyleType: "none",
    margin: "0",
    padding: "0"
  },
  caret: {
    cursor: "pointer",
    WebkitUserSelect: "none",
    MozUserSelect: "none",
    MsUserSelect: "none",
    userSelect: "none"
  },
  caret__before: {
    content: '"\\25B6"',
    color: "black",
    display: "inline-block",
    marginRight: "6px"
  },
  caret_down__before: {
    MsTransform: "rotate(90deg)",
    WebkitTransform: "rotate(90deg)",
    transform: "rotate(90deg)"
  },
  nested: {
    display: "none"
  },
  active: {
    display: "block"
  }
});

export default categoryStyle;
