const bookingCalendarStyle = {
  booking_calendar: {
    MozBoxSizing: "border-box",
    boxSizing: "border-box",
    background: "#fff",
    display: "block",
    float: "left",
    fontSize: "14px",
    marginBottom: "10px",
    width: "100%"
  },
  booking_calendar__header: {
    MozBoxSizing: "border-box",
    boxSizing: "border-box",
    background: "#2196f3",
    border: "solid 1px #e3f2fd",
    color: "#fff",
    height: "3em",
    position: "relative"
  },
  booking_calendar__header__header_content__icon_previous: {
    left: "0"
  },
  booking_calendar__header__header_content__icon_next: {
    right: "0"
  },
  booking_calendar__header__header_content__month_label: {
    left: "0",
    right: "0",
    textAlign: "center"
  },
  booking_calendar__header__header_content__icon_previous_disabled: {
    color: "#82c4f8"
  },
  booking_calendar__header__header_content__icon_next_disabled: {
    color: "#82c4f8"
  },
  booking_calendar__header__header_content__month_label_disabled: {
    color: "#82c4f8"
  },
  booking_calendar__week: {
    MozBoxSizing: "border-box",
    boxSizing: "border-box",
    backgroundColor: "#2196f3",
    borderRight: "solid 1px #e3f2fd",
    borderTop: "solid 1px #e3f2fd",
    float: "left",
    width: "100%"
  },
  booking_calendar__week_last_child: {
    borderBottom: "solid 1px #e3f2fd"
  },
  booking_calendar__week_names: {
    borderTop: "none"
  },
  booking_calendar__week_names__day_box_before: {
    paddingTop: "50%"
  },
  booking_calendar__week_names__day_box__day: {
    color: "#82c4f8",
    fontSize: "0.9em"
  },
  booking_calendar__week__day_box: {
    MozBoxSizing: "border-box",
    boxSizing: "border-box",
    float: "left",
    position: "relative",
    width: "14.2857142857%"
  },
  booking_calendar__week__day_box_before: {
    borderRight: "solid 1px #e3f2fd",
    content: '""',
    display: "block",
    paddingTop: "100%"
  },
  booking_calendar__week__day_box_first_child__day: {
    borderLeft: "solid 1px #e3f2fd"
  },
  booking_calendar__week__day_box_last_child__day: {
    borderRight: "none"
  },
  booking_calendar__week__day_box__day: {
    MozBoxSizing: "border-box",
    boxSizing: "border-box",
    background: "#2196f3",
    borderRight: "solid 1px #e3f2fd",
    bottom: "0",
    color: "#fff",
    cursor: "pointer",
    fontSize: "1em",
    left: "0",
    position: "absolute",
    right: "0",
    textAlign: "center",
    top: "0"
  },
  booking_calendar__week__day_box__day_not_clickable: {
    cursor: "default"
  },
  booking_calendar__week__day_box__day_different_month: {
    color: "#82c4f8"
  },
  booking_calendar__week__day_box__day_selected: {
    background: "#fff",
    color: "#2196f3"
  },
  booking_calendar__week__day_box__day_selected_before: {
    content: "none"
  },
  booking_calendar__week__day_box__day_selected_after: {
    content: "none"
  },
  booking_calendar__week__day_box__day_today: {
    background: "#82c4f8",
    color: "#fff"
  },
  booking_calendar__week__day_box__day_booked_day: {
    overflow: "hidden",
    zIndex: "1"
  },
  booking_calendar__week__day_box__day_booked_day_before: {
    MsTransform: "skewX(-18deg)",
    WebkitTransform: "skewX(-18deg)",
    transform: "skewX(-18deg)",
    backgroundColor: "#0960a5",
    content: '""',
    height: "100%",
    left: "18%",
    position: "absolute",
    top: "0",
    width: "100%",
    zIndex: "-1"
  },
  booking_calendar__week__day_box__day_booked_night: {
    overflow: "hidden",
    zIndex: "1"
  },
  booking_calendar__week__day_box__day_booked_night_after: {
    MsTransform: "skewX(-18deg)",
    WebkitTransform: "skewX(-18deg)",
    transform: "skewX(-18deg)",
    backgroundColor: "#0960a5",
    content: '""',
    height: "100%",
    left: "-80%",
    position: "absolute",
    top: "0",
    width: "100%",
    zIndex: "-1"
  },
  booking_calendar__week__day_box__day____day_content: {
    position: "absolute",
    top: "50%",
    left: "0",
    right: "0",
    transform: "translateY(-50%)"
  },
  booking_calendar__week_names___span: {
    color: "#82c4f8",
    fontWeight: "bold"
  },
  "@media (min-width: 600px)": {
    __expression__: "(min-width: 600px)",
    booking_calendar: {
      fontSize: "18px"
    }
  },
  "@media (min-width: 768px)": {
    __expression__: "(min-width: 768px)",
    booking_calendar: {
      fontSize: "20px"
    }
  },
  "@media (min-width: 992px)": {
    __expression__: "(min-width: 992px)",
    booking_calendar: {
      fontSize: "24px"
    }
  },
  "@media (min-width: 1200px)": {
    __expression__: "(min-width: 1200px)",
    booking_calendar: {
      fontSize: "28px"
    }
  }
};
export default bookingCalendarStyle;