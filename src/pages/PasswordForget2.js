import React, { Component } from 'react';
//import { withRouter } from 'react-router-dom';
//import { connect } from "react-redux";
import compose from "recompose/compose";

import PropTypes from 'prop-types';
// import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
// import FormControlLabel from '@material-ui/core/FormControlLabel';
// import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
// import LockIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
// import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

// import { SignUpLink } from './SignUp';
import { PasswordForgetLink } from './PasswordForget';
//import { auth } from '../firebase';
import Firebase from "../services/Firebase";
// import * as routes from '../constants/routes';

const styles = theme => ({
    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
});

const PasswordForgetPage2 = ({classes}) =>
    <div>
        <PasswordForgetForm2 classes={classes} />
    </div>

const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
});

const INITIAL_STATE = {
    email: '',
    error: null,
};

class PasswordForgetForm2 extends Component {

    constructor(props) {
        super(props);

        this.state = { ...INITIAL_STATE };
    }

    onSubmit = (event) => {
        const { email } = this.state;
        debugger;

        Firebase.doPasswordReset(email)
            .then(() => {
                debugger;
                this.setState({ ...INITIAL_STATE });
            })
            .catch(error => {
                this.setState(byPropKey('error', error));
                debugger;
            });

        event.preventDefault();
    }

    render() {
        const { classes } = this.props;
        

        const {
            email,
            error,
        } = this.state;

        const isInvalid = email === '';

        return (
            <main className={classes.main}>
                <CssBaseline />
                <Paper className={classes.paper}>
                    <form onSubmit={this.onSubmit}>

                        <FormControl margin="normal" required fullWidth>
                            <InputLabel htmlFor="email">Email</InputLabel>
                            <Input
                                value={this.state.email}
                                onChange={event => this.setState(byPropKey('email', event.target.value))}
                                id="email"
                                name="email"
                                autoComplete="email" />
                        </FormControl>
                        <Button
                            disabled={isInvalid}
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                            Reset My Password
                        </Button>

                        {error && <p>{error.message}</p>}
                    </form>
                </Paper>
            </main>
        );
    }
}

PasswordForgetForm2.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default compose(
    withStyles(styles)
)(PasswordForgetPage2);

export {
    PasswordForgetForm2,
    PasswordForgetLink,
};
