import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import { auth } from '../firebase';

const styles = theme => ({  
  button: {
      margin: theme.spacing.unit,
  },
  input: {
      display: 'none',
  }   
});

const SignOutButton = (props) =>

<Button variant="contained" color="primary" onClick={auth.doSignOut} className={props.classes.button}>
Sign Out
</Button>

export default withStyles(styles) (SignOutButton);