import * as React from "react";
import { RouteComponentProps } from "react-router-dom";
import Layout from "../components/Layout";
import Recover from "../components/Poll/Recover";

interface Props extends RouteComponentProps<void> { }

function RecoverPollPage(props: Props) {
  
  const [open, setOpen] = React.useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const handleAddTodo = () => {
    setOpen(true);
  };

  return (
    <Layout theme={null}>
      <Recover />
    </Layout>
  );
}

export default RecoverPollPage;
