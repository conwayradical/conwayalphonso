import * as React from "react";
import { RouteComponentProps } from "react-router-dom";
import Layout from "../components/Layout";
import Update from "../components/Poll/Update";

interface Props extends RouteComponentProps<void> { }

function UpdatePollPage(props: Props) {
  
  const [open, setOpen] = React.useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const handleAddTodo = () => {
    setOpen(true);
  };

  return (
    <Layout theme={null}>
      <Update />
    </Layout>
  );
}


export default UpdatePollPage;
