import React from "react";
import PropTypes from "prop-types";

class AdminDashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      //...INITIAL_STATE,
      cardAnimaton: "cardHidden",
      checked: [24, 22],
      selectedEnabled: "b",
      checkedA: true,
      checkedB: false,
      labelWidth: 0,
      imageLimit: false,
      open: false,
      ////// Form data
      company: "",
      contact_name: "",
      domain_name: "",
      already_registered: false,
      register_for_me: false,
      register_anon: false,
      webpackage: "",
      web_example1: "",
      web_example2: "",
      web_example3: ""
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.currentUser !== prevProps.currentUser) {
      this.setState({ currentUser: this.props.currentUser });
    }
  }

  componentDidMount() {
    this.setState({ currentUser: this.props.currentUser });
  }

  render() {
    const { currentUser } = this.state;
    return (
      <div>
        <h1>ADMIN DASHBOARD</h1>
        {this.state.currentUser ? (
          <div>{currentUser.company}</div>
        ) : (
          <div>Loading...</div>
        )}
      </div>
    );
  }
}

AdminDashboard.propTypes = {
  currentUser: PropTypes.object
};

export default AdminDashboard;
