import React from 'react'
import { render } from 'react-dom'
import Styles from './FormsStyles'
import { Field } from 'react-final-form'
import { TextField, Radio, Input } from 'final-form-material-ui'
import InputAdornment from '@material-ui/core/InputAdornment'
import Wizard from './Wizard'

import {
  RadioGroup,
  FormLabel,
  FormControl,
  FormControlLabel,
  LinearProgress,
} from '@material-ui/core'

import { lighten, makeStyles, withStyles } from '@material-ui/core/styles';

// components
import {
  GridContainer,
  GridItem,
  Card,
  CardHeader,
  CardBody,
  CustomInput,
  CardFooter,
  Button,
  Header,
  HeaderLinks,
} from '../components'

import Firebase, {
  getCurrentUser,
  saveSurvey,
  getSurveyData,
  siteid,
} from "../services/Firebase";

import logo from '../assets/img/helpstarter-logo-sm.png';


const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))



const Error = ({ name }) => (
  <Field
    name={name}
    subscribe={{ touched: true, error: true }}
    render={({ meta: { touched, error } }) => (touched && error ? <span>{error}</span> : null)}
  />
)

const validate = values => {
  const errors = {}
  if (!values.firstName) {
    errors.firstName = 'Required'
  }
  if (!values.lastName) {
    errors.lastName = 'Required'
  }
  if (!values.email) {
    errors.email = 'Required'
  }
  return errors
}

const BorderLinearProgress = withStyles({
  root: {
    height: 10,
    backgroundColor: lighten('#f7921e', 0.5),
  },
  bar: {
    borderRadius: 20,
    backgroundColor: '#f7921e',
  },
})(LinearProgress);

const required = value => (value ? undefined : 'Required')

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  margin: {
    margin: theme.spacing(1),
  },
}));

function UserForms(props) {
  const classes = useStyles();
  let cause = 'Development';
  const [currentUser, setCurrentUser] = React.useState(Firebase.auth().currentUser);
  // read the data in for this survey so it can be loaded into the form
  // this can be muliple surveys all split op by their survey name. Load 
  // correspening survey into its controlled form
  //const [error, setError] = React.useState(false)
  const [loading, setLoading] = React.useState(true)
  const[investmentData, setInvestmentData] = React.useState([]);
  const [history, setHistory] = React.useState(props.props.history);
  const [progress, setProgress] = React.useState(0);
  const [surveyLengthData, setSurveyLengthData] = React.useState(0);
  // const[investmentData, setInvestmentData] = React.useState(getSurveyData(siteid, currentUser.uid, 'investment'));
  // const[investmentData, setInvestmentData] = React.useState(getSurveyData(siteid, currentUser.uid, 'investment'));


  React.useEffect(() => {
    // code to run on component mount
    let isSubscribed = true
    if (isSubscribed) {
      getSurveyData(siteid, currentUser.uid, 'investment').then(res => {
        if(res){
          const data = res[0];
          setInvestmentData(data);
        }
        setLoading(false);
      });
    }
    return () => isSubscribed = false;
  }, [])

  const onSubmit = async values => {
    await sleep(300)
    
    const currentUser = Firebase.auth().currentUser;
    getCurrentUser(currentUser.uid).then(res => {
      const userdetails = res;
      // console.log('userdetails', userdetails);

      if(values.id === null){
        delete values.id;
      }
      
      values.uid = currentUser.uid;
      values.siteid = siteid;
      values.email = userdetails.email;
      values.username = userdetails.username;
      values.lastname = userdetails.lastname;

      //setTimeout(() => {
        // window.alert(JSON.stringify(values, 0, 2));
        saveSurvey(siteid, values);
      //}, 1000);
      
      

    })
    
    // return history.push("/home");
  }

  const journey =(values) => {
    const thediv = (values+1)/surveyLengthData;
    const perc = (thediv*100);
    setProgress(perc);
  }

  const surveyLength = (value) => {
    setSurveyLengthData(value);
  }

  return (
    <div>
      <Styles>
        <h1>Investment Survey</h1>
        <h4><img src={logo} alt="Logo" /></h4>
        <a href="/account">{currentUser.email}</a>
        <GridContainer justify="center">
        {loading ? (
          <LinearProgress />
        ) : (
          <Wizard
            initialValues={{ 
              id: investmentData.id || null,
              survey: investmentData.survey || 'investment', 
              investmoney: investmentData.investmoney || 'yes', 
              savemoney: investmentData.savemoney || 'yes', 
              cause: investmentData.cause || 'Development',
              invest: investmentData.invest || '',
              interest: investmentData.interest || '',
              projects: investmentData.projects || '',
              investmoney2: investmentData.investmoney2 || '',
            }}
            validate={validate}
            onSubmit={onSubmit}
            journey={journey}
            surveyLength={surveyLength}
          >
            <Wizard.Page>
              <GridItem xs={12} sm={12} md={12}>
                <FormControl component="fieldset">
                  <FormLabel component="legend">
                    If you had the opportunity to save money, and learn about investment would you?
                  </FormLabel>
                  <RadioGroup row>
                    <FormControlLabel
                      label="Yes"
                      control={<Field name="savemoney" component={Radio} type="radio" value="yes" />}
                    />
                    <FormControlLabel
                      label="No"
                      control={<Field name="savemoney" component={Radio} type="radio" value="no" />}
                    />
                  </RadioGroup>
                </FormControl>
              </GridItem>
            </Wizard.Page>

            <Wizard.Page>
              <GridItem xs={12} sm={12} md={12}>
                <FormControl component="fieldset">
                  <FormLabel component="legend">
                    Would you like to invest your money and get a big return within six months, to a year?
                  </FormLabel>
                  <RadioGroup value="yes" row>
                    <FormControlLabel
                      label="Yes"
                      control={<Field name="investmoney" component={Radio} type="radio" value="yes" />}
                    />
                    <FormControlLabel
                      label="No"
                      control={<Field name="investmoney" component={Radio} type="radio" value="no" />}
                    />
                  </RadioGroup>
                </FormControl>
              </GridItem>
            </Wizard.Page>

            {/* 
          Please chose what type of cause? a) Development ____ or b) Humanitarian
_____ or other type, name it: _____________________________________
          */}

            <Wizard.Page>
              <GridItem xs={12} sm={12} md={12}>
                <FormControl component="fieldset">
                  <FormLabel component="legend">Please choose what type of cause</FormLabel>
                  <RadioGroup name="cause" value={cause} row>
                    <FormControlLabel
                      label="Development"
                      control={<Field name="cause" component={Radio} type="radio" value="Development" />}
                    />
                    <FormControlLabel
                      label="Humanitarian"
                      control={<Field name="cause" component={Radio} type="radio" value="Humanitarian" />}
                    />
                  </RadioGroup>
                  <Field required name="cause" component={TextField} type="text" label="Cause" />
                  <Error name="cause" />
                </FormControl>
              </GridItem>
            </Wizard.Page>

            {/* How much would you start with to invest? $________________ */}

            <Wizard.Page>
              <GridItem xs={12} sm={12} md={12}>
                <FormControl component="fieldset">
                  <FormLabel component="legend">How much would you start with to invest?</FormLabel>
                  <Field
                    id="adornment-amount"
                    name="invest"
                    component={Input}
                    type="number"
                    label="$ Investment"
                    startAdornment={<InputAdornment position="start">$</InputAdornment>}
                    validate={required}
                  />
                  <Error name="invest" />
                </FormControl>
              </GridItem>
              <GridItem xs={12} sm={12} md={12}>
                <div>&nbsp;</div>
              </GridItem>
              {/* How much interest would you like to received back? _________% */}
              <GridItem xs={12} sm={12} md={12}>
                <FormControl component="fieldset">
                  <FormLabel component="legend">How much interest would you like to received back?</FormLabel>
                  <Field
                    id="adornment-interest"
                    name="interest"
                    component={Input}
                    type="number"
                    label="Interest"
                    endAdornment={<InputAdornment position="end">%</InputAdornment>}
                    validate={required}
                  />
                  <Error name="interest" />
                </FormControl>
              </GridItem>
            </Wizard.Page>

            {/* Do you have a special project(s), or cause(s), you are passionate about
developing or needing funds for? */}

            <Wizard.Page>
              <GridItem xs={12} sm={12} md={12}>
                <FormControl component="fieldset">
                  <FormLabel component="legend">
                    Do you have a special project(s), or cause(s), you are passionate about developing or needing funds
                    for?
                  </FormLabel>
                  <Field required name="projects" component={TextField} type="text" label="Projects / Causes" />
                  <Error name="projects" />
                </FormControl>
              </GridItem>

              {/* How much interest would you like to received back? _________% */}
              <GridItem xs={12} sm={12} md={12}>
                <div>&nbsp;</div>
              </GridItem>
              <GridItem xs={12} sm={12} md={12}>
                <FormControl component="fieldset">
                  <FormLabel component="legend">
                    If you had a chance to invest into a great opportunity, and it only cost, between $100, to $500, a
                    month, would you?
                  </FormLabel>
                  <RadioGroup row>
                    <FormControlLabel
                      label="Yes"
                      control={<Field name="investmoney2" component={Radio} type="radio" value="yes" />}
                    />
                    <FormControlLabel
                      label="No"
                      control={<Field name="investmoney2" component={Radio} type="radio" value="no" />}
                    />
                  </RadioGroup>
                </FormControl>
              </GridItem>
            </Wizard.Page>

            {/* How much interest would you like to received back? _________% */}

          </Wizard>)}
          <GridItem xs={12} sm={12} md={12}>
            <BorderLinearProgress
              className={classes.margin}
              variant="determinate"
              color="secondary"
              value={progress}
            />
          </GridItem>
        </GridContainer>
      </Styles>
    </div>
  )
}

export default UserForms
