import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import bookingCalendarStyle from "assets/jss/BookingCalendar";

const styles = () => ({
  ...bookingCalendarStyle
});

const Day = props => (
  <div className={props.classes.day_box}>
    <div className={props.classes.day} onClick={props.clickHandler}>
      <div className={props.classes.day_content}>{props.children}</div>
    </div>
  </div>
);

Day.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  clickHandler: PropTypes.func
};

Day.defaultProps = {
  className: ""
};

export default withStyles(styles)(Day);
