import React from 'react';
import Day from './Day';
import { withStyles } from "@material-ui/core/styles";
import bookingCalendarStyle from "assets/jss/BookingCalendar";

const styles = () => ({
  ...bookingCalendarStyle
});

const DayNames = (props) => (
  <div className={props.classes.week}>
    <Day>{'Mon'}</Day>
    <Day>{'Tue'}</Day>
    <Day>{'Wed'}</Day>
    <Day>{'Thu'}</Day>
    <Day>{'Fri'}</Day>
    <Day>{'Sat'}</Day>
    <Day>{'Sun'}</Day>
  </div>
);

export default withStyles(styles)(DayNames);
