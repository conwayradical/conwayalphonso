import React from "react";
import { Link } from "react-router-dom";

import PropTypes from "prop-types";
import classnames from "classnames";
// import bytes from "bytes";

import Grid from "@material-ui/core/Grid";
import Hidden from "@material-ui/core/Hidden";

import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
// import CardFooter from "../components/Card/CardFooter.jsx";

import Button from "components/CustomButtons/Button.jsx";

// @material-ui/icons
import { AccountBox } from "@material-ui/icons";

import Table from "@material-ui/core/Table";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";

import LinearProgress from "@material-ui/core/LinearProgress";

// import FileMenu from "components/FileMenu";
// import FileIcon from "components/FileIcon";
import FilesAddButton from "components/FilesAddButton";
// import FilesTableToolbar from "components/FilesTableToolbar";

import { withStyles } from "@material-ui/core/styles";

import headerLinksStyle from "assets/jss/material-kit-react/components/headerLinksStyle.jsx";

//import Firebase, { firestore, usersFS } from "services/Firebase";
import { firestore } from "services/Firebase";

const styles = theme => ({
  root: {
    width: "100%"
  },
  table: {},
  tableWrapper: {
    overflowX: "auto"
  },
  tableRowName: {
    paddingRight: 0
  },
  tableRowNameIcon: {
    paddingRight: 16,
    color: theme.palette.primary.dark
  },
  tableRowNameText: {
    paddingTop: 5
  },
  notFound: {
    paddingTop: 32
  },
  notFoundImage: {
    width: 320,
    opacity: 0.5
  },
  tableColumnWidth: {
    width: "25%",
    maxWidth: 200,
    overflow: "hidden"
  },
  adminNavLink: {
    color: "#000",
    textDecoration: "none",
    "&:hover": {
      color: "#000"
    }
  },
  ...headerLinksStyle
});

class AdminClientTable extends React.Component {
  state = {
    users: [],
    usersSource: [],
    ready: false,
    search: ""
  };

  constructor(props) {
    super(props);
    this.handleSearch = this.handleSearch.bind(this);
  }

  componentDidMount() {
    this.unsubscribe = firestore()
      .collection(`users`)
      //.orderBy("created", "desc")
      .onSnapshot(querySnapshot => {
        let users = [];
        querySnapshot.forEach(doc => {
          let user = doc.data();
          users.push(Object.assign({ id: doc.id }, user));
        });
        this.setState({ users: users, ready: true });
      });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  handleSearch(search) {
    if (search) {
      this.setState({
        users: this.state.usersSource.filter(user => {
          return user.name.indexOf(search) > -1;
        })
      });
    } else {
      this.setState({ users: this.state.usersSource });
    }
  }

  /**
   * Component Story
   * ===============
   * AdminClientTable
   * Company/Product Name | client name | client email | Detail Button
   * 
   * Detail Button will open Modal with client details info including everything entered 
   * into the prospect form and listing all the clients files
   * Admi Client Details has all user fields including compnay name, domain, product, package etc...
   * 
   *  <Layout>
        <ClientDetailsForm />
        <ClientFilesTable />
      </Layout>
   *
   */

  render() {
    const { classes } = this.props;
    const hStyle = {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
      fontWeight: "300",
      lineHeight: "1.5em"
    };
    return this.state.ready ? (
      <div className={classes.root}>
        <Card className={classes[this.state.cardAnimaton]}>
          <CardHeader color="primary" className={classes.cardHeader}>
            <h4 style={hStyle}>CLIENT ADMIN</h4>
          </CardHeader>
          <CardBody>
            {/*<FilesTableToolbar onSearch={this.handleSearch} />*/}
            <div className={classes.tableWrapper}>
              <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <TableCell>Book</TableCell>
                    <Hidden xsDown smDown>
                      <TableCell>Name</TableCell>
                    </Hidden>
                    <Hidden xsDown smDown>
                      <TableCell>Email</TableCell>
                    </Hidden>
                    <TableCell />
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.state.users.map(user => {
                    return (
                      <TableRow key={user.id}>
                        <TableCell
                          className={classnames([
                            classes.tableRowName,
                            classes.tableColumnWidth
                          ])}
                        >
                          <Grid container direction={"row"}>
                            <Grid item className={classes.tableRowNameIcon}>
                              {/*<FileIcon type={user.type} />*/}
                            </Grid>
                            <Grid item className={classes.tableRowNameText}>
                              {user.company}
                            </Grid>
                          </Grid>
                        </TableCell>
                        <Hidden xsDown smDown>
                          <TableCell
                            className={classnames([
                              classes.tableRowName,
                              classes.tableColumnWidth
                            ])}
                          >
                            {user.contact_name}
                          </TableCell>
                        </Hidden>
                        <Hidden xsDown>
                          <TableCell
                            className={classnames([
                              classes.tableRowName,
                              classes.tableColumnWidth
                            ])}
                          >
                            {user.email}
                          </TableCell>
                        </Hidden>
                        <TableCell numeric>
                          <Link
                            to={`/admin/${user.id}`}
                            className={classnames([
                              classes.listItem,
                              classes.adminNavLink
                            ])}
                          >
                            <Button
                              color="transparent"
                              target="_blank"
                              className={classnames([
                                classes.navLink,
                                classes.adminNavLink
                              ])}
                            >
                              <AccountBox className={classes.icons} /> Details
                            </Button>
                          </Link>
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </div>
          </CardBody>
        </Card>
        <FilesAddButton />
      </div>
    ) : (
      <LinearProgress />
    );
  }
}

AdminClientTable.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AdminClientTable);
