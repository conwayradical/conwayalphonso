import React from "react";
import { connect } from "react-redux";
import compose from "recompose/compose";
// import classnames from "classnames";
import PropTypes from "prop-types";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// import InputAdornment from "@material-ui/core/InputAdornment";
import CustomTabs from "components/CustomTabs/CustomTabs.jsx";
import ProspectForm from "components/ProspectForm/ProspectForm.jsx";
import EcommerceAdmin from "components/EcommerceAdmin/EcommerceAdmin.jsx";

// import Icon from "@material-ui/core/Icon";
//import FormControlLabel from "@material-ui/core/FormControlLabel";
//import FormControl from "@material-ui/core/FormControl";
//import InputLabel from "@material-ui/core/InputLabel";
//import Select from "@material-ui/core/Select";
//import MenuItem from "@material-ui/core/MenuItem";
import Snackbar from "@material-ui/core/Snackbar";
//import Input from "@material-ui/core/Input";
import IconButton from "@material-ui/core/IconButton";

// @material-ui/icons
// import Email from "@material-ui/icons/Email";
//import People from "@material-ui/icons/People";
//import Work from "@material-ui/icons/Work";
//import Domain from "@material-ui/icons/Domain";
//import Checkbox from "@material-ui/core/Checkbox";
//import Check from "@material-ui/icons/Check";
import CloseIcon from "@material-ui/icons/Close";
import Create from "@material-ui/icons/Create";
import Map from "@material-ui/icons/Map";
import SpeakerNotes from "@material-ui/icons/SpeakerNotes";
import Shop from "@material-ui/icons/Shop";

import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
//import Button from "components/CustomButtons/Button.jsx";
//import Card from "components/Card/Card.jsx";
//import CardBody from "components/Card/CardBody.jsx";
//import CardHeader from "components/Card/CardHeader.jsx";
//import CardFooter from "components/Card/CardFooter.jsx";
//import CustomInput from "components/CustomInput/CustomInput.jsx";
import AdminDashboard from "components/AdminDashboard/AdminDashboard.jsx";

import dashboardStyle from "assets/jss/material-kit-react/views/dashboardStyle.jsx";
import basicsStyle from "assets/jss/material-kit-react/views/componentsSections/basicsStyle.jsx";

//import image from "assets/img/bg7.jpg";

import { userFS, doUpdateUserData } from "services/Firebase";

const prospectStyle = {
  ...dashboardStyle,
  ...basicsStyle,
  formControl: {
    width: "100%"
  }
};

//const byPropKey = (propertyName, value) => () => ({
//  [propertyName]: value
//});

class AdminClientDetails extends React.Component {
  constructor(props) {
    super(props);
    // we use this to make the card to appear after the page has been rendered
    this.state = {
      ...INITIAL_STATE,
      cardAnimaton: "cardHidden",
      checked: [24, 22],
      selectedEnabled: "b",
      checkedA: true,
      checkedB: false,
      labelWidth: 0,
      imageLimit: false,
      open: false,
      ////// Form data
      company: "",
      contact_name: "",
      domain_name: "",
      already_registered: false,
      register_for_me: false,
      register_anon: false,
      webpackage: "",
      web_example1: "",
      web_example2: "",
      web_example3: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleToUpdate = this.handleToUpdate.bind(this);
  }
  componentDidMount() {
    // we add a hidden class to the card and after 700 ms we delete it and the transition appears
    setTimeout(
      function() {
        this.setState({ cardAnimaton: "" });
      }.bind(this),
      700
    );
    if (!this.props.user) {
      this.setState({ loading: true });
    } else {
      return;
    }

    this.unsubscribe = userFS(this.props.match.params.id).onSnapshot(
      snapshot => {
        const clientUser = snapshot.data();
        this.setState({
          ...snapshot.data(),
          clientUser: clientUser,
          loading: false
        });
      }
    );
  }
  componentWillUnmount() {
    this.unsubscribe();
  }

  handleChange = event => {
    // console.log("ooo event", event);
    this.setState({ [event.target.name]: event.target.value });
  };
  handleChangeEnabled(event) {
    this.setState({ selectedEnabled: event.target.value });
  }
  handleToggle(value) {
    const { checked } = this.state;
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    this.setState({
      checked: newChecked
    });
  }

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    this.setState({ open: false });
  };

  handleToUpdate = eventForm => {
    //const { authUser } = this.props; //does this exist here

    let {
      company,
      contact_name,
      domain_name,
      checked,
      already_registered,
      register_for_me,
      register_anon,
      webpackage,
      web_example1,
      web_example2,
      web_example3
    } = this.state;
    eventForm.preventDefault();

    checked.includes(1)
      ? (already_registered = true)
      : (already_registered = false);
    checked.includes(2) ? (register_for_me = true) : (register_for_me = false);
    checked.includes(3) ? (register_anon = true) : (register_anon = false);

    // const { history } = this.props;

    let data = {
      id: this.state.id,
      company,
      contact_name,
      checked,
      domain_name,
      already_registered,
      register_for_me,
      register_anon,
      webpackage,
      web_example1,
      web_example2,
      web_example3
    };
    //debugger;
    this.unregister = doUpdateUserData(data);
    this.setState({ open: true });
    this.setState({ ...INITIAL_STATE });
  };

  render() {
    const { classes } = this.props;

    return (
      <div>
        <GridContainer justify="center">
          <GridItem xs={12} sm={12} md={12}>
            <CustomTabs
              title="Web Client Type:"
              headerColor="primary"
              tabs={[
                {
                  tabName: "Admin Dashboard",
                  tabIcon: Create,
                  tabContent: (
                    <AdminDashboard currentUser={this.state.clientUser} />
                  )
                },
                {
                  tabName: "Author",
                  tabIcon: Create,
                  tabContent: (
                    <ProspectForm currentUser={this.state.clientUser} />
                  )
                },
                {
                  tabName: "Brouchure",
                  tabIcon: Map,
                  tabContent: <div>Brouchure site details</div>
                },
                {
                  tabName: "Blog",
                  tabIcon: SpeakerNotes,
                  tabContent: <div>Blog site details</div>
                },
                {
                  tabName: "E-Commerce",
                  tabIcon: Shop,
                  tabContent: (
                    <EcommerceAdmin currentUser={this.state.clientUser} />
                  )
                }
              ]}
            />
          </GridItem>
        </GridContainer>
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left"
          }}
          open={this.state.open}
          autoHideDuration={6000}
          onClose={this.handleClose}
          ContentProps={{
            "aria-describedby": "message-id"
          }}
          message={<span id="message-id">Updated</span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.close}
              onClick={this.handleClose}
            >
              <CloseIcon />
            </IconButton>
          ]}
        />
      </div>
    );
  }
}
const INITIAL_STATE = {
  email: "",
  title: "",
  description: "",
  content: "",
  author: "",
  date: "",
  categories: "",
  tags: "",
  ilist: [],
  error: null
};

AdminClientDetails.propTypes = {
  history: PropTypes.object,
  authUser: PropTypes.object,
  classes: PropTypes.object,
  rest: PropTypes.object,
  user: PropTypes.object,
  match: PropTypes.object
};

const mapStateToProps = state => ({
  users: state.userState.users,
  // articles: state.articleState.articles,
  authUser: state.sessionState.authUser,
  editArticle: state.editArticleState.editArticle,
  articles: state.firestore.ordered.articles
    ? state.firestore.ordered.articles
    : [],
  boards: state.firestore.ordered.boards ? state.firestore.ordered.boards : []
});

const mapDispatchToProps = dispatch => ({
  onSetUsers: users => dispatch({ type: "USERS_SET", users }),
  onSetArticles: articles => dispatch({ type: "ARTICLES_SET", articles }),
  onEditArticle: editArticle => dispatch({ type: "EDIT_ARTICLE", editArticle })
});

// const authCondition = authUser => !!authUser && authUser.role === "ADMIN";

export default compose(
  //withAuthorization(authCondition),
  withStyles(prospectStyle),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(AdminClientDetails);
