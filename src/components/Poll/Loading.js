import React from 'react';
//import CircularProgress from 'material-ui/CircularProgress';
import LinearProgress from "@material-ui/core/LinearProgress";

export default (props) => (
    <div className="text-xs-center" style={props.loading ? {} : { display: 'none' }}>
        <br />
        <LinearProgress />
    </div>
);